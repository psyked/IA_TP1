﻿namespace VacuumAgent
{
    public enum Actions
    {
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Vacuum,
        PickUpJewel
    }
}